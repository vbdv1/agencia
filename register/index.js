'use strict'

const port = process.env.PORT || 3023;
const URL_DB = "mongodb+srv://victor:victorSD@cluster0.vjs95.mongodb.net/usuarios"
const jwt = require("jsonwebtoken");
const ip = require('./config').ip;
const https = require('https');
const fs = require('fs');

const express = require('express');
const logger = require('morgan');
const mongojs = require('mongojs');
const passService = require('../services/pass.service');
const bcrypt = require('bcrypt');

const app = express();
const HTTPS_OPTIONS = {
    key: fs.readFileSync('../cert/key.pem'),
    cert: fs.readFileSync('../cert/cert.pem')
};

var db = mongojs(URL_DB);
var id = mongojs.ObjectID;

//Declaramos nuestros middleware
app.use(logger('dev'));
app.use(express.json());


// Creamos nuestros Middleware

//Declaramos nuestras rutas y controladores


app.post('/api/register', (req, res, next) => {
    const usuario= req.body;
    const queColeccion = req.params.colecciones;

    if( !usuario.nombre || !usuario.email || !usuario.password ){
        res.status(400).json({
            error: 'Bad data',
            description: 'Se precisa al menos campos <nombre>, <email>, <password> '
        });
    } else {
        db.usuarios.findOne({email: req.body.email }, (err, elemento) => {
            if(err) return next(err);   //Propagamos el error

            if(elemento == null){ //no está registrado anteriormente
                res.status(200).json({
                    result: 'Usuario registrado correctamente',
                    coleccion: queColeccion,
                    elemento: elemento
                });
            } else { //email ya registrado
                res.status(400).json({
                    error: 'Registro no válido',
                    description: 'Email ya registrado en nuestra base de datos'
                });
            }
            
        })        
    }
});  






//Lanzamos el servicio en modo seguro
https.createServer(HTTPS_OPTIONS, app )
.listen(port, () => {
    console.log(`API-REGISTER ejecutándose en https://${ip}:${port}/api/register`);
});