'use strict'

const port = process.env.PORT || 3022;
const URL_DB = "mongodb+srv://victor:victorSD@cluster0.vjs95.mongodb.net/usuarios"
const ip = require('./config').ip;
const bcrypt = require('bcrypt');
const https = require('https');
const fs = require('fs');
const passService = require('../services/pass.service');
const express = require('express');
const logger = require('morgan');
const mongojs = require('mongojs');
const tokenService = require('../services/token.service');
const app = express();
const SECRET = require('../config').secret;

const HTTPS_OPTIONS = {
    key: fs.readFileSync('../cert/key.pem'),
    cert: fs.readFileSync('../cert/cert.pem')
};


var db = mongojs(URL_DB);
var id = mongojs.ObjectID;

//Declaramos nuestros middleware
app.use(logger('dev'));
app.use(express.urlencoded({extended: false}));
app.use(express.json());


//Declaramos nuestras rutas y controladores


app.post('/api/login', (req, res, next) => {
    const usuario= req.body;
    const queColeccion = req.params.colecciones;

    if( !usuario.email || !usuario.password ){
        res.status(400).json({
            error: 'Bad data',
            description: 'Se precisa al menos campos <email>, <password> '
        });
    } else {

        db.usuarios.findOne({email: req.body.email}, (err, usuarioDB) => {
            if (err) {
                return res.status(500).json({
                    result: 'error'
                });
            }

            if (!usuarioDB) {
                return res.status(400).json({
                    result: 'No existe el usuario'
                });
            }
            const equal = bcrypt.compareSync(usuario.password, usuarioDB.password);
            if (!equal){
                return res.status(400).json({
                    result: 'Contraseña incorrecta.'
                });
            }
            
            let token = tokenService.creaToken(usuario);
            res.status(200).json({
                result: 'Login correcto'
            });
            console.log(token);
            
           
        })
    }
});





//Lanzamos el servicio en modo seguro
https.createServer(HTTPS_OPTIONS, app )
.listen(port, () => {
    console.log(`API-LOGIN ejecutándose en https://${ip}:${port}/api/login`);
});