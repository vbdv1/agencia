'use strict'

const bcrypt = require('bcrypt');

// Función que deuelve un hash con un salt incluido en el formato: 
//  $2b$15$v9igM.f0dmeCBngfxgIKDu/HQQMpqliCpZu5dl1XYbO80Y1yKySV.y
//  ............................./*******************************
//  Alg cost      Salt                    Hash

function encriptaPassword( password) {
    return bcrypt.hash( password, 10);
}

function comparaPassword( password, hash){
    return bcrypt.compare( password, hash);
}

module.exports = {
    encriptaPassword, 
    comparaPassword
};
