'use strict'

const port = process.env.PORT || 3021;
const URL_DB = "mongodb+srv://victor:victorSD@cluster0.vjs95.mongodb.net/pagos"


const https = require('https');
const fs = require('fs');
const ip=require('./config').ip;
const jwt = require('jwt-simple');
const moment = require('moment');
const SECRET = require('../config').secret;
const { json } = require('express');

const express = require('express');
const logger = require('morgan');
const mongojs = require('mongojs');

const app = express();
const HTTPS_OPTIONS = {
    key: fs.readFileSync('../cert/key.pem'),
    cert: fs.readFileSync('../cert/cert.pem')
};


var db = mongojs(URL_DB);
var id = mongojs.ObjectID;

//Declaramos nuestros middleware
app.use(logger('dev'));
app.use(express.urlencoded({extended: false}));
app.use(express.json());


// Creamos nuestros Middleware
function auth(req, res, next) {
    if ( !req.headers.authorization ){
        res.status(403).json({
            result: "KO",
            mensaje: "No se ha enviado el token Bearer en la cabecera Authorization"
        });
        return next();
    }

    const token = req.headers.authorization.split(" ")[1];
    try{
        const payload = jwt.decode( token, SECRET, false); 

        if (payload.exp < moment().unix()){ //token caducado
            res.status(401).json({
                result: "KO",
                mensaje: "El token ha caducado"
            });
            return next();
        }
    }catch(err){
        return res.status(401).json({ //Error de token no válido
            result: "KO",
            mensaje: "Acceso no autorizado a este servicio"
        });
    } 
        req.params.token = token;
        return next();
}

//Declaramos nuestras rutas y controladores

app.get('/api', (req, res, next) => {
    console.log('GET /api');
    console.log(req.params);
    console.log(req.collection);
    

    db.getCollectionNames((err, colecciones) => {
        if(err) return next(err);   //Propagamos el error

        console.log(colecciones);
        res.json({
            result: 'OK',
            colecciones: colecciones
        });
    }); 
});  

app.get('/api/pagos', (req, res, next) => {

    db.pagos.find((err, elementos) => {
        if(err) return next(err);   //Propagamos el error

        console.log(elementos);
        res.json({
            result: 'OK',
            coleccion: req.params.colecciones,
            elementos: elementos
        });
    }); 
});  

app.get('/api/pagos/:id', (req, res, next) => {

    db.pagos.findOne( { _id: id(req.params.id) }, (err, elemento) => {
        if(err) return next(err);   //Propagamos el error

        console.log(elemento);
        res.json({
            result: 'OK',
            coleccion: req.params.colecciones,
            elemento: elemento
        });
    }); 
});  

app.post('/api/pagos', auth, (req, res, next) => {
    const nuevoElemento = req.body;
    const queColeccion = req.params.colecciones;

    if( !nuevoElemento.nombre || !nuevoElemento.numTarjeta || !nuevoElemento.fechaCad ||
        !nuevoElemento.codSeg || !nuevoElemento.importe){
        res.status(400).json({
            error: 'Bad data',
            description: 'Se precisa al menos campos <nombre>, <numTarjete>, <fechaCad>, <codSeg>, <importe> '
        });
    } else {
        db.pagos.save(nuevoElemento, (err, elementoGuardado) => {
            if (err) return next(err);

            console.log(elementoGuardado);
            res.json({
                result: 'OK',
                coleccion: queColeccion,
                elemento: nuevoElemento
            });
        });
    }
});

app.put('/api/pagos/:id', auth, (req, res, next) => {
    let elementoID = req.params.id;
    let nuevosDatos = req.body;

    db.pagos.update(
        { _id: id(elementoID) }, 
        {$set: nuevosDatos},
        { safe: true, multi: false},
        (err, resultado) => {
            if(err) return next(err);

            console.log(resultado);
            res.json({
                result: 'OK',
                coleccion: req.params.colecciones,
                elemento: nuevosDatos
            });
    });
});

app.delete('/api/pagos/:id', auth, (req, res, next) => {
    let elementoID = req.params.id;
    let queColeccion = req.params.colecciones;

    db.pagos.remove({_id: id(elementoID)}, (err, resultado) => {
        if(err) return next(err);

        console.log(resultado);
        res.json({
            result: "OK, Deleted",
            coleccion: queColeccion,
            elementoID: elementoID
        });


    });
});

//Lanzamos el servicio en modo seguro
https.createServer(HTTPS_OPTIONS, app )
.listen(port, () => {
    console.log(`API PAGOS ejecutándose en https://${ip}:${port}/api/pagos`);
});