'use strict'

const port = process.env.PORT || 3002;
const URL_DB = "lmongodb+srv://victor:victorSD@cluster0.vjs95.mongodb.net/vehiculos"


const https = require('https');
const fs = require('fs');
const ip=require('./config').ip;
const express = require('express');
const logger = require('morgan');
const mongojs = require('mongojs');
const jwt = require('jwt-simple');
const { json } = require('express');
const SECRET = require('../config').secret;
const moment = require('moment');

const app = express();
const HTTPS_OPTIONS = {
    key: fs.readFileSync('../cert/key.pem'),
    cert: fs.readFileSync('../cert/cert.pem')
};


var db = mongojs(URL_DB);
var id = mongojs.ObjectID;

//Declaramos nuestros middleware
app.use(logger('dev'));
app.use(express.urlencoded({extended: false}));
app.use(express.json());


// Creamos nuestros Middleware
function auth(req, res, next) {
    if ( !req.headers.authorization ){
        res.status(403).json({
            result: "KO",
            mensaje: "No se ha enviado el token Bearer en la cabecera Authorization"
        });
        return next();
    }

    const token = req.headers.authorization.split(" ")[1];
    try{
        const payload = jwt.decode( token, SECRET, false); 

        if (payload.exp < moment().unix()){ //token caducado
            res.status(401).json({
                result: "KO",
                mensaje: "El token ha caducado"
            });
            return next();
        }
    }catch(err){
        return res.status(401).json({ //Error de token no válido
            result: "KO",
            mensaje: "Acceso no autorizado a este servicio"
        });
    } 
        req.params.token = token;
        return next();
}

//Declaramos nuestras rutas y controladores

app.get('/api/vehiculos', (req, res, next) => {

    db.vehiculos.find((err, elementos) => {
        if(err) return next(err);   //Propagamos el error

        console.log(elementos);
        res.json({
            result: 'OK',
            coleccion: req.params.colecciones,
            elementos: elementos
        });
    }); 
});  

app.get('/api/vehiculos/:id', (req, res, next) => {

    db.vehiculos.findOne( { _id: id(req.params.id) }, (err, elemento) => {
        if(err) return next(err);   //Propagamos el error

        console.log(elemento);
        res.json({
            result: 'OK',
            coleccion: req.params.colecciones,
            elemento: elemento
        });
    }); 
});  

app.post('/api/vehiculos', auth, (req, res, next) => {
    const nuevoElemento = req.body;
    const queColeccion = req.params.colecciones;

    if( !nuevoElemento.marca || !nuevoElemento.tipo || !nuevoElemento.modelo ||
        !nuevoElemento.duracion || !nuevoElemento.precio){
        res.status(400).json({
            error: 'Bad data',
            description: 'Se precisa al menos campos <marca>, <modelo>, <tipo>, <duracion>, <precio> '
        });
    } else {
        db.vehiculos.save(nuevoElemento, (err, elementoGuardado) => {
            if (err) return next(err);

            console.log(nuevoElemento);
            res.status(200).json({
                result: 'OK',
                coleccion: queColeccion,
                elemento: nuevoElemento
            });
        });
    }
});

app.put('/api/vehiculos/:id', auth, (req, res, next) => {
    let elementoID = req.params.id;
    let nuevosDatos = req.body;

    db.vehiculos.update(
        { _id: id(elementoID) }, 
        {$set: nuevosDatos},
        { safe: true, multi: false},
        (err, resultado) => {
            if(err) return next(err);

            console.log(resultado);
            res.json({
                result: 'OK',
                coleccion: req.params.colecciones,
                elemento: nuevosDatos
            });
    });
});

app.delete('/api/vehiculos/:id', auth, (req, res, next) => {
    let elementoID = req.params.id;
    let queColeccion = req.params.colecciones;

    db.vehiculos.remove({_id: id(elementoID)}, (err, resultado) => {
        if(err) return next(err);

        console.log(resultado);
        res.json({
            result: "OK, Deleted",
            coleccion: queColeccion,
            elementoID: elementoID
        });


    });
});

//Lanzamos el servicio en modo seguro
https.createServer(HTTPS_OPTIONS, app )
.listen(port, () => {
    console.log(`API VEHICULOS ejecutándose en https://${ip}:${port}/api/vehiculos`);
});