'use strict'

const port = process.env.PORT || 3200;
const express = require('express');
const logger = require('morgan');
const fetch = require('node-fetch');
const URL_DB = "mongodb+srv://victor:victorSD@cluster0.vjs95.mongodb.net/usuarios"
const mongojs = require('mongojs');
const jwt = require('jwt-simple');
const SECRET = require('../config').secret;
const moment = require('moment');
const bcrypt = require('bcrypt');
const passService = require('../services/pass.service');

const ipvuelos=require('./config').ipvuelos;
const ipresvuelos=require('./config').ipresvuelos;
const ipvehiculos=require('./config').ipvehiculos;
const ipresvehiculos=require('./config').ipresvehiculos;
const iphoteles=require('./config').iphoteles;
const ipreshoteles=require('./config').ipreshoteles;
const ippagos=require('./config').ippagos;
const iplogin=require('./config').iplogin;
const ipregister=require('./config').ipregister;
const ipgw=require('./config').ipgw;

const TokenService = require('../services/token.service');
const fs = require('fs');
const https = require('https');
const HTTPS_OPTIONS = {
    key: fs.readFileSync('../cert/key.pem'),
    cert: fs.readFileSync('../cert/cert.pem')
};
var db = mongojs(URL_DB);
const app = express();



//Declaramos nuestros middleware
app.use(logger('dev'));
app.use(express.urlencoded({extended: false}));
app.use(express.json());
process.env.NODE_TLS_REJECT_UNAUTHORIZED = "0";


// Creamos nuestros Middleware

function auth(req, res, next) {
    if ( !req.headers.authorization ){
        res.status(403).json({
            result: "KO",
            mensaje: "No se ha enviado el token Bearer en la cabecera Authorization"
        });
        return next();
    }

    const token = req.headers.authorization.split(" ")[1];
    try{
        const payload = jwt.decode( token, SECRET, false); 

        if (payload.exp < moment().unix()){ //token caducado
            res.status(401).json({
                result: "KO",
                mensaje: "El token ha caducado"
            });
            return next();
        }
    }catch(err){
        return res.status(401).json({ //Error de token no válido
            result: "KO",
            mensaje: "Acceso no autorizado a este servicio"
        });
    } 
        req.params.token = token;
        return next();
}

//*********************************************************/
//Declaramos nuestras rutas y controladores
//*********************************************************/

//API-VUELOS (PORT = 3001)

app.get('/api/vuelos', (req, res, next) => {
    const queURL= `https://${ipvuelos}:3001/api/vuelos`;
    

    fetch (queURL )
        .then( resp => resp.json() )
        .then( json => {
            //Mi lógica de negocio
            res.json({
                result:'OK',
                coleccion: 'vuelos',
                elementos: json.elementos
            });
        })
        .catch(err => { 
            res.json(err)
        });
});  


app.get('/api/vuelos/:id', (req, res, next) => {

    const queID= req.params.id;
    const queURL= `https://${ipvuelos}:3001/api/vuelos/${queID}`;
    
    fetch (queURL, {
        method: 'GET',
        headers: {
            'Content-Type': 'application/json', 
        }
    })
        .then( resp => resp.json() )
        .then( json => {
            //Mi lógica de negocio
            res.json({
                result:'OK',
                coleccion: 'vuelos',
                elemento: json.elemento
            });
        })
        .catch(err => { 
            res.json(err)
        });
});  

app.post('/api/vuelos', auth, (req, res, next) => {
    const nuevoElemento = req.body;
    const queURL= `https://${ipvuelos}:3001/api/vuelos`;
    const queToken = req.params.token;
    fetch( queURL, {
                method: 'POST',
                body: JSON.stringify(nuevoElemento),
                headers: {
                       'Content-Type': 'application/json',
                        'Authorization': `Bearer ${queToken}`  
                    }
    })
        .then(resp => resp.json() )
        .then(json => {
            //Mi lógica de negocio
            if( !nuevoElemento.origen || !nuevoElemento.destino || !nuevoElemento.fechaSal ||
                !nuevoElemento.fechaLle || !nuevoElemento.precio || !nuevoElemento.compañia) {
                res.status(400).json({
                    error: 'Bad data',
                    description: 'Se precisa al menos campos <origen>, <destino>, <fechaSal>, <fechaLle>, <precio>, <compañia>'
                });
            } else {
                res.json({
                    result: 'OK',
                    coleccion: 'vuelos',
                    elemento: json.elemento
                })
            }
        })
        .catch(err => { 
            res.json(err)
        });
    }); 


app.put('/api/vuelos/:id', auth, (req, res, next) => {
    let queID = req.params.id;
    let nuevoElemento = req.body;
    const queURL= `https://${ipvuelos}:3001/api/vuelos/${queID}`;
    const queToken = req.params.token;
    fetch( queURL, {
                method: 'PUT',
                body: JSON.stringify(nuevoElemento),
                headers: {
                       'Content-Type': 'application/json',
                        'Authorization': `Bearer ${queToken}`  
                    }
    })
        .then(resp => resp.json() )
        .then(json => {
            //Mi lógica de negocio
            res.json({
                result: 'OK',
                coleccion: 'vuelos',
                elemento: json.elemento
            })
        })
        .catch(err => { 
            res.json(err)
        });
});

app.delete('/api/vuelos/:id', auth, (req, res, next) => {
    const queID= req.params.id;
    const queURL= `https://${ipvuelos}:3001/api/vuelos/${queID}`;
    const queToken = req.params.token;
    fetch (queURL, {
        method: 'DELETE',
        headers: {
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${queToken}` 
        }
    })
        .then( resp => resp.json() )
        .then( json => {
            //Mi lógica de negocio
            res.json({
                result:'OK, Deleted',
                coleccion: 'vuelos',
            });
        })
        .catch(err => { 
            res.json(err)
        });
});


//API-VEHICULOS (PORT = 3002)

app.get('/api/vehiculos', (req, res, next) => {
    const queURL= `https://${ipvehiculos}:3002/api/vehiculos`;

    fetch (queURL, {
        method: 'GET',
        headers: {
            'Content-Type': 'application/json', 
        }
    })
        .then( resp => resp.json() )
        .then( json => {
            //Mi lógica de negocio
            res.json({
                result:'OK',
                coleccion: 'vehiculos',
                elementos: json.elementos
            });
        })
        .catch(err => { 
            res.json(err)
        });
});  


app.get('/api/vehiculos/:id', (req, res, next) => {

    const queID= req.params.id;
    const queURL= `https://${ipvehiculos}:3002/api/vehiculos/${queID}`;
    
    fetch (queURL, {
        method: 'GET',
        headers: {
            'Content-Type': 'application/json', 
        }
    })
        .then( resp => resp.json() )
        .then( json => {
            //Mi lógica de negocio
            res.json({
                result:'OK',
                coleccion: 'vehiculos',
                elemento: json.elemento
            });
        })
        .catch(err => { 
            res.json(err)
        });
});  

app.post('/api/vehiculos', auth, (req, res, next) => {
    const nuevoElemento = req.body;
    const queURL= `https://${ipvehiculos}:3002/api/vehiculos`;
    const queToken = req.params.token;
    fetch( queURL, {
                method: 'POST',
                body: JSON.stringify(nuevoElemento),
                headers: {
                       'Content-Type': 'application/json',
                        'Authorization': `Bearer ${queToken}`  
                    }
        })
        .then(resp => resp.json() )
        .then(json => {
            //Mi lógica de negocio
            if( !nuevoElemento.marca || !nuevoElemento.tipo || !nuevoElemento.modelo ||
                !nuevoElemento.duracion || !nuevoElemento.precio){
                res.status(400).json({
                    error: 'Bad data',
                    description: 'Se precisa al menos campos <marca>, <modelo>, <tipo>, <duracion>, <precio> '
                });
            } else {
                res.json({
                    result: 'OK',
                    coleccion: 'vehiculos',
                    elemento: json.elemento
                });
            }
        })
        .catch(err => { 
            res.json(err)
        });
});


app.put('/api/vehiculos/:id', auth, (req, res, next) => {
    let queID = req.params.id;
    let nuevoElemento = req.body;
    const queURL= `https://${ipvehiculos}:3002/api/vehiculos/${queID}`;
    const queToken = req.params.token;
    fetch( queURL, {
                method: 'PUT',
                body: JSON.stringify(nuevoElemento),
                headers: {
                       'Content-Type': 'application/json',
                        'Authorization': `Bearer ${queToken}`  
                    }
    })
        .then(resp => resp.json() )
        .then(json => {
            //Mi lógica de negocio
            res.json({
                result: 'OK',
                coleccion: 'vehiculos',
                elemento: json.elemento
            })
            
        })
        .catch(err => { 
            res.json(err)
        });

});

app.delete('/api/vehiculos/:id', auth, (req, res, next) => {
    const queID= req.params.id;
    const queURL= `https://${ipvehiculos}:3002/api/vehiculos/${queID}`;
    const queToken = req.params.token;

    fetch (queURL, {
        method: 'DELETE',
        headers: {
            'Content-Type': 'application/json', 
            'Authorization': `Bearer ${queToken}`
        }
    })
        .then( resp => resp.json() )
        .then( json => {
            //Mi lógica de negocio
            res.json({
                result:'OK, Deleted',
                coleccion: 'vehiculos',
            });
        })
        .catch(err => { 
            res.json(err)
        });
});


//API-HOTELES (PORT = 3003)

app.get('/api/hoteles', (req, res, next) => {
    const queURL= `https://${iphoteles}:3003/api/hoteles`;

    fetch (queURL, {
        method: 'GET',
        headers: {
            'Content-Type': 'application/json', 
        }
    })
        .then( resp => resp.json() )
        .then( json => {
            //Mi lógica de negocio
            res.json({
                result:'OK',
                coleccion: 'hoteles',
                elementos: json.elementos
            });
        })
        .catch(err => { 
            res.json(err)
        });
});  


app.get('/api/hoteles/:id', (req, res, next) => {

    const queID= req.params.id;
    const queURL= `https://${iphoteles}:3003/api/hoteles/${queID}`;
    
    fetch (queURL, {
        method: 'GET',
        headers: {
            'Content-Type': 'application/json', 
        }
    })
        .then( resp => resp.json() )
        .then( json => {
            //Mi lógica de negocio
            res.json({
                result:'OK',
                coleccion: 'hoteles',
                elemento: json.elemento
            });
        })
        .catch(err => { 
            res.json(err)
        });
});  

app.post('/api/hoteles', auth, (req, res, next) => {
    const nuevoElemento = req.body;
    const queURL= `https://${iphoteles}:3003/api/hoteles`;
    const queToken = req.params.token;
    fetch( queURL, {
                method: 'POST',
                body: JSON.stringify(nuevoElemento),
                headers: {
                       'Content-Type': 'application/json',
                        'Authorization': `Bearer ${queToken}`  
                    }
    })
        .then(resp => resp.json() )
        .then(json => {
            //Mi lógica de negocio
            if( !nuevoElemento.entrada || !nuevoElemento.salida || !nuevoElemento.regimen ||
                !nuevoElemento.tipoHabitacion || !nuevoElemento.precio){
                res.status(400).json({
                    error: 'Bad data',
                    description: 'Se precisa al menos campos <entrada>, <salida>, <regimen>, <tipoHabitacion>, <precio> '
                });
            } else {
                res.json({
                    result: 'OK',
                    coleccion: 'hoteles',
                    elemento: json.elemento
                })
            }
            
        })
        .catch(err => { 
            res.json(err)
        });
});


app.put('/api/hoteles/:id', auth, (req, res, next) => {
    let queID = req.params.id;
    let nuevoElemento = req.body;
    const queURL= `https://${iphoteles}:3003/api/hoteles/${queID}`;
    const queToken = req.params.token;
    fetch( queURL, {
                method: 'PUT',
                body: JSON.stringify(nuevoElemento),
                headers: {
                       'Content-Type': 'application/json',
                        'Authorization': `Bearer ${queToken}`  
                    }
    })
        .then(resp => resp.json() )
        .then(json => {
            //Mi lógica de negocio
            res.json({
                result: 'OK',
                coleccion: 'hoteles',
                elemento: json.elemento
            })
            
        })
        .catch(err => { 
            res.json(err)
        });

});

app.delete('/api/hoteles/:id', auth, (req, res, next) => {
    const queID= req.params.id;
    const queURL= `https://${iphoteles}:3003/api/hoteles/${queID}`;
    const queToken = req.params.token;

    fetch (queURL, {
        method: 'DELETE',
        headers: {
            'Content-Type': 'application/json', 
            'Authorization': `Bearer ${queToken}`
        }
    })
        .then( resp => resp.json() )
        .then( json => {
            //Mi lógica de negocio
            res.json({
                result:'OK, Deleted',
                coleccion: 'hoteles',
            });
        })
        .catch(err => { 
            res.json(err)
        });
});


//API-RESVUELOS (PORT = 3011)

app.get('/api/resvuelos', (req, res, next) => {
    const queURL= `https://${ipresvuelos}:3011/api/resvuelos`;

    fetch (queURL, {
        method: 'GET',
        headers: {
            'Content-Type': 'application/json', 
        }
    })
        .then( resp => resp.json() )
        .then( json => {
            //Mi lógica de negocio
            res.json({
                result:'OK',
                coleccion: 'resvuelos',
                elementos: json.elementos
            });
        })
        .catch(err => { 
            res.json(err)
        });
});  


app.get('/api/resvuelos/:id', (req, res, next) => {

    const queID= req.params.id;
    const queURL= `https://${ipresvuelos}:3011/api/resvuelos/${queID}`;
    
    fetch (queURL, {
        method: 'GET',
        headers: {
            'Content-Type': 'application/json', 
        }
    })
        .then( resp => resp.json() )
        .then( json => {
            //Mi lógica de negocio
            res.json({
                result:'OK',
                coleccion: 'resvuelos',
                elementos: json.elementos
            });
        })
        .catch(err => { 
            res.json(err)
        });
});  

app.post('/api/resvuelos', auth, (req, res, next) => {
    const nuevoElemento = req.body;
    const queURL= `https://${ipresvuelos}:3011/api/resvuelos`;
    const queToken = req.params.token;
    fetch( queURL, {
                method: 'POST',
                body: JSON.stringify(nuevoElemento),
                headers: {
                       'Content-Type': 'application/json',
                        'Authorization': `Bearer ${queToken}`  
                    }
        })
        .then(resp => resp.json() )
        .then(json => {
            //Mi lógica de negocio
            if( !nuevoElemento.origen || !nuevoElemento.destino || !nuevoElemento.fechaSal ||
                !nuevoElemento.fechaLle || !nuevoElemento.precio || !nuevoElemento.compañia) {
                res.status(400).json({
                    error: 'Bad data',
                    description: 'Se precisa al menos campos <origen>, <destino>, <fechaSal>, <fechaLle>, <precio>, <compañia>'
                });
            } else {
                res.status(200).json({
                    result: 'OK',
                    coleccion: 'resvuelos',
                    elemento: json.elemento
                })
            }
            
        })
        .catch(err => { 
            res.json(err)
        });
 
});


app.put('/api/resvuelos/:id', auth, (req, res, next) => {
    let queID = req.params.id;
    let nuevoElemento = req.body;
    const queURL= `https://${ipresvuelos}:3011/api/resvuelos/${queID}`;
    const queToken = req.params.token;
    fetch( queURL, {
                method: 'PUT',
                body: JSON.stringify(nuevoElemento),
                headers: {
                       'Content-Type': 'application/json',
                        'Authorization': `Bearer ${queToken}`  
                    }
    })
        .then(resp => resp.json() )
        .then(json => {
            //Mi lógica de negocio
            res.json({
                result: 'OK',
                coleccion: 'resvuelos',
                elemento: json.elemento
            })
            
        });

});

app.delete('/api/resvuelos/:id', auth, (req, res, next) => {
    const queID= req.params.id;
    const queURL= `https://${ipresvuelos}:3011/api/resvuelos/${queID}`;
    const queToken = req.params.token;

    fetch (queURL, {
        method: 'DELETE',
        headers: {
            'Content-Type': 'application/json', 
            'Authorization': `Bearer ${queToken}`
        }
    })
        .then( resp => resp.json() )
        .then( json => {
            //Mi lógica de negocio
            res.json({
                result:'OK, Deleted',
                coleccion: 'resvuelos',
            });
        });
});


//API-RESVEHICULOS (PORT = 3012)

app.get('/api/resvehiculos', (req, res, next) => {
    const queURL= `https://${ipresvehiculos}:3012/api/resvehiculos`;

    fetch (queURL, {
        method: 'GET',
        headers: {
            'Content-Type': 'application/json', 
        }
    })
        .then( resp => resp.json() )
        .then( json => {
            //Mi lógica de negocio
            res.json({
                result:'OK',
                coleccion: 'resvehiculos',
                elementos: json.elementos
            });
        });
});  


app.get('/api/resvehiculos/:id', (req, res, next) => {

    const queID= req.params.id;
    const queURL= `https://${ipresvehiculos}:3012/api/resvehiculos/${queID}`;
    
    fetch (queURL, {
        method: 'GET',
        headers: {
            'Content-Type': 'application/json', 
        }
    })
        .then( resp => resp.json() )
        .then( json => {
            //Mi lógica de negocio
            res.json({
                result:'OK',
                coleccion: 'resvehiculos',
                elementos: json.elementos
            });
        });
});  

app.post('/api/resvehiculos', auth, (req, res, next) => {
    const nuevoElemento = req.body;
    const queURL= `https://${ipresvehiculos}:3012/api/resvehiculos`;
    const queToken = req.params.token;
    fetch( queURL, {
                method: 'POST',
                body: JSON.stringify(nuevoElemento),
                headers: {
                       'Content-Type': 'application/json',
                        'Authorization': `Bearer ${queToken}`  
                    }
        })
        .then(resp => resp.json() )
        .then(json => {
            //Mi lógica de negocio
            //Mi lógica de negocio
            if( !nuevoElemento.marca || !nuevoElemento.tipo || !nuevoElemento.modelo ||
                !nuevoElemento.duracion || !nuevoElemento.precio){
                res.status(400).json({
                    error: 'Bad data',
                    description: 'Se precisa al menos campos <marca>, <modelo>, <tipo>, <duracion>, <precio> '
                });
            } else {
                res.json({
                    result: 'OK',
                    coleccion: 'resvehiculos',
                    elemento: nuevoElemento
                });
            }
        });
 
});


app.put('/api/resvehiculos/:id', auth, (req, res, next) => {
    let queID = req.params.id;
    let nuevoElemento = req.body;
    const queURL= `https://${ipresvehiculos}:3012/api/resvehiculos/${queID}`;
    const queToken = req.params.token;
    fetch( queURL, {
                method: 'PUT',
                body: JSON.stringify(nuevoElemento),
                headers: {
                       'Content-Type': 'application/json',
                        'Authorization': `Bearer ${queToken}`  
                    }
    })
        .then(resp => resp.json() )
        .then(json => {
            //Mi lógica de negocio
            res.json({
                result: 'OK',
                coleccion: 'resvehiculos',
                elemento: json.elemento
            })
            
        });

});

app.delete('/api/resvehiculos/:id', auth, (req, res, next) => {
    const queID= req.params.id;
    const queURL= `https://${ipresvehiculos}:3012/api/resvehiculos/${queID}`;
    const queToken = req.params.token; 

    fetch (queURL, {
        method: 'DELETE',
        headers: {
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${queToken}` 
        }
    })
        .then( resp => resp.json() )
        .then( json => {
            //Mi lógica de negocio
            res.json({
                result:'OK, Deleted',
                coleccion: 'resvehiculos',
            });
        });
});


//API-RESHOTELES (PORT = 3013  )

app.get('/api/reshoteles', (req, res, next) => {
    const queURL= `https://${ipreshoteles}:3013/api/reshoteles`;

    fetch (queURL, {
        method: 'GET',
        headers: {
            'Content-Type': 'application/json', 
        }
    })
        .then( resp => resp.json() )
        .then( json => {
            //Mi lógica de negocio
            res.json({
                result:'OK',
                coleccion: 'reshoteles',
                elementos: json.elementos
            });
        });
});  


app.get('/api/reshoteles/:id', (req, res, next) => {

    const queID= req.params.id;
    const queURL= `https://${ipreshoteles}:3013/api/reshoteles/${queID}`;
    
    fetch (queURL, {
        method: 'GET',
        headers: {
            'Content-Type': 'application/json', 
        }
    })
        .then( resp => resp.json() )
        .then( json => {
            //Mi lógica de negocio
            res.json({
                result:'OK',
                coleccion: 'reshoteles',
                elementos: json.elementos
            });
        });
});  

app.post('/api/reshoteles', auth, (req, res, next) => {
    const nuevoElemento = req.body;
    const queURL= `https://${ipreshoteles}:3013/api/reshoteles`;
    const queToken = req.params.token;
    fetch( queURL, {
                method: 'POST',
                body: JSON.stringify(nuevoElemento),
                headers: {
                       'Content-Type': 'application/json',
                        'Authorization': `Bearer ${queToken}`  
                    }
        })
        .then(resp => resp.json() )
        .then(json => {
            //Mi lógica de negocio
            //Mi lógica de negocio
            if( !nuevoElemento.entrada || !nuevoElemento.salida || !nuevoElemento.regimen ||
                !nuevoElemento.tipoHabitacion || !nuevoElemento.precio){
                res.status(400).json({
                    error: 'Bad data',
                    description: 'Se precisa al menos campos <entrada>, <salida>, <regimen>, <tipoHabitacion>, <precio> '
                });
            } else {
                res.json({
                    result: 'OK',
                    coleccion: 'hoteles',
                    elemento: json.elemento
                })
            }
        });
 
});


app.put('/api/reshoteles/:id', auth, (req, res, next) => {
    let queID = req.params.id;
    let nuevoElemento = req.body;
    const queURL= `https://${ipreshoteles}:3013/api/reshoteles/${queID}`;
    const queToken = req.params.token;
    fetch( queURL, {
                method: 'PUT',
                body: JSON.stringify(nuevoElemento),
                headers: {
                       'Content-Type': 'application/json',
                        'Authorization': `Bearer ${queToken}`  
                    }
    })
        .then(resp => resp.json() )
        .then(json => {
            //Mi lógica de negocio
            res.json({
                result: 'OK',
                coleccion: 'reshoteles',
                elemento: json.elemento
            })
            
        });

});

app.delete('/api/reshoteles/:id', auth, (req, res, next) => {
    const queID= req.params.id;
    const queURL= `https://${ipreshoteles}:3013/api/reshoteles/${queID}`;
    const queToken = req.params.token;

    fetch (queURL, {
        method: 'DELETE',
        headers: {
            'Content-Type': 'application/json', 
            'Authorization': `Bearer ${queToken}`
        }
    })
        .then( resp => resp.json() )
        .then( json => {
            //Mi lógica de negocio
            res.json({
                result:'OK, Deleted',
                coleccion: 'reshoteles',
            });
        });
});



//API-PAGOS (PORT = 3021)

app.get('/api/pagos', (req, res, next) => {
    const queURL= `https://${ippagos}:3021/api/pagos`;

    fetch (queURL, {
        method: 'GET',
        headers: {
            'Content-Type': 'application/json', 
        }
    })
        .then( resp => resp.json() )
        .then( json => {
            //Mi lógica de negocio
            res.json({
                result:'OK',
                coleccion: 'pagos',
                elementos: json.elementos
            });
        });
});  


app.get('/api/pagos/:id', (req, res, next) => {

    const queID= req.params.id;
    const queURL= `https://${ippagos}:3021/api/pagos/${queID}`;
    
    fetch (queURL, {
        method: 'GET',
        headers: {
            'Content-Type': 'application/json', 
        }
    })
        .then( resp => resp.json() )
        .then( json => {
            //Mi lógica de negocio
            res.json({
                result:'OK',
                coleccion: 'pagos',
                elementos: json.elementos
            });
        });
});  

app.post('/api/pagos', auth, (req, res, next) => {
    const nuevoElemento = req.body;
    const queURL= `https://${ippagos}:3021/api/pagos`;
    const queToken = req.params.token;
    fetch( queURL, {
                method: 'POST',
                body: JSON.stringify(nuevoElemento),
                headers: {
                       'Content-Type': 'application/json',
                        'Authorization': `Bearer ${queToken}`  
                    }
        })
        .then(resp => resp.json() )
        .then(json => {
            //Mi lógica de negocio
            if( !nuevoElemento.nombre || !nuevoElemento.numTarjeta || !nuevoElemento.fechaCad ||
                !nuevoElemento.codSeg || !nuevoElemento.importe){
                res.status(400).json({
                    error: 'Bad data',
                    description: 'Se precisa al menos campos <nombre>, <numTarjete>, <fechaCad>, <codSeg>, <importe> '
                });
            } else {
                res.json({
                    result: 'OK',
                    coleccion: 'pagos',
                    elemento: nuevoElemento
                });
            }
            
        });
 
});


app.put('/api/pagos/:id', auth, (req, res, next) => {
    let queID = req.params.id;
    let nuevoElemento = req.body;
    const queURL= `https://${ippagos}:3021/api/pagos/${queID}`;
    const queToken = req.params.token;
    fetch( queURL, {
                method: 'PUT',
                body: JSON.stringify(nuevoElemento),
                headers: {
                       'Content-Type': 'application/json',
                        'Authorization': `Bearer ${queToken}`  
                    }
    })
        .then(resp => resp.json() )
        .then(json => {
            //Mi lógica de negocio
            res.json({
                result: 'OK',
                coleccion: 'pagos',
                elemento: json.elemento
            });
            
        });

});

app.delete('/api/pagos/:id', auth, (req, res, next) => {
    const queID= req.params.id;
    const queURL= `https://${ippagos}:3021/api/pagos/${queID}`;
    const queToken = req.params.token;

    fetch (queURL, {
        method: 'DELETE',
        headers: {
            'Content-Type': 'application/json', 
            'Authorization': `Bearer ${queToken}`
        }
    })
        .then( resp => resp.json() )
        .then( json => {
            //Mi lógica de negocio
            res.json({
                result:'OK, Deleted',
                coleccion: 'pagos',
            });
        });
});


//API-LOGIN (PORT = 3022)

app.post('/api/login', (req, res, next) => {
    const usuario = req.body;
    const queURL= `https://${iplogin}:3022/api/login`;
    fetch( queURL, {
                method: 'POST',
                body: JSON.stringify(usuario),
                headers: {
                       'Content-Type': 'application/json'
                }
        })
        .then(resp => resp.json() )
        .then(json => {
            //Mi lógica de negocio

            if( !usuario.email || !usuario.password ){
                res.status(400).json({
                    error: 'Bad data',
                    description: 'Se precisa al menos campos <email>, <password> '
                });
            } else {
                db.usuarios.findOne({email: req.body.email}, (err, usuarioDB) => {
                    if (err) {
                        return res.status(500).json({
                            result: 'error'
                        });
                    }
        
                    if (!usuarioDB) {
                        return res.status(400).json({
                            result: 'No existe el usuario'
                        });
                    }
                    const equal = bcrypt.compareSync(usuario.password, usuarioDB.password); //la de la bd está hasheada
                    if (!equal){
                        return res.status(400).json({
                            result: 'Contraseña incorrecta.'
                        });
                    }
                    res.status(200).json({
                        result: 'Login correcto'
                    });
                })
            }
        });
 
});


//API-REGISTER (PORT = 3023)

app.post('/api/register', (req, res, next) => {
    const usuario = req.body;
    const queURL= `https://${ipregister}:3023/api/register`;
    fetch( queURL, {
                method: 'POST',
                body: JSON.stringify(usuario),
                headers: {
                       'Content-Type': 'application/json'  
                    }
        })
        .then(resp => resp.json() )
        .then(json => {
            //Mi lógica de negocio
            if( !usuario.nombre || !usuario.email || !usuario.password ){
                res.status(400).json({
                    error: 'Bad data',
                    description: 'Se precisa al menos campos <nombre>, <email>, <password> '
                });
            } else {
                db.usuarios.findOne({email: req.body.email }, (err, elemento) => {
                    if(err) return next(err);   //Propagamos el error
                    
                    if(elemento == null){ //no está registrado anteriormente
                        passService.encriptaPassword(usuario.password) //Encriptamos la password para que no aparezca la palabra en la base de datos
                            .then(hash => {
                                usuario.password = hash;
                                console.log(usuario.password);
                                db.usuarios.save(usuario, (err, elementoGuardado) => {
                                    if (err) return next(err);
                                    res.status(200).json({
                                        result: 'Usuario registrado correctamente',
                                        coleccion: 'usuarios',
                                        elemento: elementoGuardado
                                    });
                                });
                            });
                    } else { //email ya registrado
                        res.status(400).json({
                            error: 'Registro no válido',
                            description: 'Email ya registrado en nuestra base de datos'
                        });
                    }
                    
                })   
            }
        });
 
});

https.createServer(HTTPS_OPTIONS, app )
.listen(port, () => {
    console.log(`API GATEWAY ejecutándose en http://${ipgw}:${port}/api/{tabla}`);
});
