// Fichero de configuración para información relativa a generar el token
// Está aquí para que sea accesible por todos los servicios
module.exports = {
    secret: 'mipalabrasecreta',
    tokenExpTime: 24*60          // 1 día expresados en minutos
};